import XCTest
@testable import Bonus

final class BonusTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Bonus().text, "Hello, World!")
    }
}
